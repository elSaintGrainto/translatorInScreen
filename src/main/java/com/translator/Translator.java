
/*
 * Copyright 2016 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.translator;

import com.google.cloud.translate.Detection;
import com.google.cloud.translate.Language;
import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.LanguageListOption;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;
import com.google.common.collect.ImmutableList;
import java.io.PrintStream;
import java.util.List;

public class Translator {
    private String apikey  = "AIzaSyBlQA798Y252WItctghAPg1IbyEFB8gz7Q";
    private String url = "";
    private String path = "";
    private String sourceLang = "";
    private String targetLang = "";

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }


    public Translator() {
        this.sourceLang = "es";
        this.targetLang = "en";
    }
    public Translator(String sourceLang,String targetLang) {
        this.sourceLang = sourceLang;
        this.targetLang = targetLang;
    }
  /**
   * Detect the language of input text.
   *
   * @param sourceText source text to be detected for language
   * @param out print stream
   */
  //[START translate_detect_language]
  public List detectLanguage(String sourceText, PrintStream out) {
    Translate translate = createTranslateService();
    List<Detection> detections = translate.detect(ImmutableList.of(sourceText));
    return detections;
  }
  //[END translate_detect_language]

  /**
   * Translates the source text in any language to English.
   *
   * @param sourceText source text to be translated
   * @param out print stream
   */
  //[START translate_translate_text]
  public String translateText(String sourceText) {
    Translate translate = createTranslateService();
    Translation translation = translate.translate(sourceText);
    return translation.getTranslatedText();
  }
  //[END translate_translate_text]

  /**
   * Translate the source text from source to target language.
   * Make sure that your project is whitelisted.
   *
   * @param sourceText source text to be translated
   * @param sourceLang source language of the text
   * @param targetLang target language of translated text
   * @param out print stream
   */
  //[START translate_text_with_model]
  public String translateTextWithOptionsAndModel(
      String sourceText,
      String sourceLang,
      String targetLang,
      PrintStream out) {

    Translate translate = createTranslateService();
    TranslateOption srcLang = TranslateOption.sourceLanguage(sourceLang);
    TranslateOption tgtLang = TranslateOption.targetLanguage(targetLang);

    // Use translate `model` parameter with `base` and `nmt` options.
    TranslateOption model = TranslateOption.model("nmt");

    Translation translation = translate.translate(sourceText, srcLang, tgtLang, model);
    return translation.getTranslatedText();
  }
  //[END translate_text_with_model]


  /**
   * Translate the source text from source to target language.
   *
   * @param sourceText source text to be translated
   * @param sourceLang source language of the text
   * @param targetLang target language of translated text
   * @param out print stream
   */
  public String translateTextWithOptions(
      String sourceText,
      String sourceLang,
      String targetLang) {

    Translate translate = createTranslateService();
    TranslateOption srcLang = TranslateOption.sourceLanguage(sourceLang);
    TranslateOption tgtLang = TranslateOption.targetLanguage(targetLang);
    Translation translation = translate.translate(sourceText, srcLang, tgtLang);
    return translation.getTranslatedText();
  }

  /**
   * Displays a list of supported languages and codes.
   *
   * @param out print stream
   * @param tgtLang optional target language
   */
  //[START translate_list_language_names]
  //[START translate_list_codes]
  public List getSupportedLanguages(String srcLang) {
    Translate translate = createTranslateService();
    LanguageListOption target = LanguageListOption.targetLanguage(srcLang);
   List<Language> languages = translate.listSupportedLanguages(target);

    return languages;
  }
  //[END translate_list_codes]
  //[END translate_list_language_names]

  /**
   * Create Google Translate API Service.
   *
   * @return Google Translate Service
   */
  public  Translate createTranslateService() {
    return TranslateOptions.newBuilder().setApiKey(getApikey()).setTargetLanguage(targetLang).build().getService();
  }

}
