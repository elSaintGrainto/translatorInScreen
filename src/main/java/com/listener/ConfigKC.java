/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.JSONObject;



/**
 *
 * @author juan
 */
public class ConfigKC {

    List<String> CommandList = new ArrayList<>();
    List<String> ParamList = new ArrayList<>();
    int keyEndRun;
    int keyTrigger;
    boolean robotWritting;
    boolean run ;
    boolean WriteWithRobot;
    private File config;
    private JSONObject json;
    private  KeyCapture ke;
    private String path;
    
    public static String readFile(String filename) {
        String result = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch(IOException e) {
        }
        return result;
    }
    /**
     * Define the propertys to add into the json, and then make a file with this
     */
    private void setJson(org.json.JSONObject  jsonTmp){
        if(json.length()==0){// set to default json
//            JSONObject tr = new JSONObject();
//            tr.put("sourceLang",ke.getTranslator().getSourceLang());
//            tr.put("targetLang",ke.getTranslator().getTargetLang());
//            tr.put("apikey",ke.getTranslator().getApikey());
//            json.put("translator", tr);
            json.put("writeWithRobot",ke.writeWithRobot);
            json.put("keyEndRun",ke.getKeyEndRun());
            json.put("keyTrigger",ke.getKeyTrigger());
            JSONArray Cmd = new JSONArray();
            ke.commandList.forEach((el) -> {
                Cmd.add(el);
            });
            JSONArray Prm = new JSONArray(); 
            ke.paramList.forEach((el) -> {
                Prm.add(el);
            });
            json.put("commandList", Cmd);
            json.put("paramList", Prm);
            return;
        }
        this.json = jsonTmp;
        
    }
    private static void setJsonOfClass(Class classObj){
        for(Field field : classObj.getDeclaredFields()){
            String name = field.getName();
            field.setAccessible(true);
            System.out.println("name="+name);
        }
        for(Constructor c: classObj.getConstructors()){
            
        }
    }
    public JSONObject getJson(){
        return json;
    }
    public  void writeConfigFile(){
        //setting the config file to write with the configuration file
        FileWriter writer = null;
        try {
            //writting in the file
            writer = new FileWriter(path+"config.json");
            writer.write(json.toString(4));
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    public void setConfigurationInClass(Object className){
        Class cl = className.getClass();
        for ( Field field :cl.getDeclaredFields()){
            String name = field.getName();
            //get the field of the class and try to change the value of it
            if(json.has(name)){
                String type = field.getType().getTypeName();
                if(type.contains("List")) {
                    org.json.JSONArray obj = json.getJSONArray(name);
                    ArrayList<String> list = new ArrayList<>();
                    for (int i = 0; i < obj.length(); i++) {
                        list.add(obj.getString(i));
                    }
                    try {//changing field of class
                        field.setAccessible(true);
                        field.set(className,list);
                        System.out.println("field changedF");
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else if(type.equals("object")){
//                        try {
//                            field.setAccessible(true);
//                            field.set(className, json.getBoolean(name));
//                            System.out.println("field changedF");
//                        } catch (IllegalArgumentException | IllegalAccessException ex) {
//                            Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
//                        }
                }else if(type.equals("boolean")){
                    try {
                        field.setAccessible(true);
                        field.set(className, json.getBoolean(name));
                        System.out.println("field changedF");
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }else if(type.equals("int")){
                    try {
                       field.setAccessible(true);
                       field.set(className, json.getInt(name));
                       System.out.println("field changedF");
                   } catch (IllegalArgumentException | IllegalAccessException ex) {
                       Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
                   }
                }else if(type.equals("string")){
                    try {
                       field.setAccessible(true);
                       field.set(className, json.getString(name));
                       System.out.println("field changedF");
                   } catch (IllegalArgumentException | IllegalAccessException ex) {
                       Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
                   }
                } 
                System.out.println("typo="+type);
            }
        }
    }
 
    public static void main(String[] args) throws IOException{
        KeyCapture k = new KeyCapture();
        setJsonOfClass( k.getClass());
        //testing
        //creating a json in execution
//        KeyCapture ke= new KeyCapture();
//        JSONArray Cmd = new  JSONArray();
//        JSONArray Par = new  JSONArray();
//        
//        FileWriter writer = null;
//        JSONObject json= new JSONObject();
//        
//        json.put("writeWithRobot",true);
//        json.put("KeyEndRun",545);
//        json.put("KeyTrigger",765);
//        for(String el: ke.CommandList)
//            Cmd.add(el);
//        for(String el: ke.ParamList)
//            Par.add(el);
//        json.put("CommandList", Cmd);
//        json.put("ParamList", Par);
////        save the json in json file
//        try {
//            writer = new FileWriter("config.json");
//            writer.write(json.toString(4));
//            System.out.println(json.toString(4));
//        } catch (IOException ex) {
//            Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
//        } finally{
//            writer.flush();
//            writer.close();
//        }
        //writting in the file
          
//        JSONObject json = null;
//        KeyCapture ke = new  KeyCapture();
//        Class cl = ke.getClass();
//        //set reader from file
//        //convert the file to json
//        json = new JSONObject(readFile("config.json"));
//        System.out.println("json=" + json.toString(4));
//        for ( Field field :cl.getDeclaredFields()){
//                String name = field.getName();
//                System.out.println(" name="+name);
//                //get the field of the class and try to change the value of it
//                if(json.has(name)){
//                    boolean isArray = field.getGenericType().getTypeName().contains("List");
//                    if(isArray) {
//                        org.json.JSONArray obj = json.getJSONArray(name);
//                        ArrayList<String> list = new ArrayList<>();
//                        for (int i = 0; i < obj.length(); i++) {
//                            list.add(obj.getString(i));
//                        }
//                        try {//changing field of class
//                            field.setAccessible(true);
//                            field.set(ke,list);
//                            System.out.println("field changedF");
//                        } catch (IllegalArgumentException | IllegalAccessException ex) {
//                            Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                    else{
//                        if(field.getType().getTypeName().equals("boolean")){
//                            try {
//                                field.setAccessible(true);
//                                field.set(ke, json.getBoolean(name));
//                                System.out.println("field changedF");
//                            } catch (IllegalArgumentException | IllegalAccessException ex) {
//                                Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                        }
//                        if(field.getType().getTypeName().equals("int")){
//                             try {
//                                field.setAccessible(true);
//                                field.set(ke, json.getInt(name));
//                                System.out.println("field changedF");
//                            } catch (IllegalArgumentException | IllegalAccessException ex) {
//                                Logger.getLogger(ConfigKC.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                        }
//                    }
//
//                }
//        
//    }
    }
}
