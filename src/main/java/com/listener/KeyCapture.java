/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listener;

import com.jm.translatorinscreen.GoogleTranslator;
import java.awt.AWTException;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;

/**
 *
 * @author juan
 */
public class KeyCapture implements Runnable {

    private String dataCaptured;
    private String tmpData;
    private String translatedText = "";
    private GoogleTranslator translator;
    public ArrayList<String> commandList = new ArrayList<>();
    public ArrayList<String> paramList = new ArrayList<>();
    private int keyEndRun;
    private int keyTrigger;
    private boolean robotWritting;
    private int enterKey;
    private boolean run;
    private boolean isWritting;
    private boolean endWritting;
    public boolean writeWithRobot;
    private static String title = "############################Key-Capture Configuration##########################";
    private UtilsFunctions ut = new UtilsFunctions();

    public KeyCapture() {
        this.keyEndRun = 48;//0
        this.keyTrigger = 13;//Enter
        this.enterKey = this.keyTrigger;
        this.isWritting = false;
        this.endWritting = false;
        this.robotWritting = false;
        this.dataCaptured = "";
        this.tmpData = "";
        this.commandList.add("-tr");
        this.commandList.add("/tr");
        this.writeWithRobot = true;
        this.translator= new GoogleTranslator();
        writeConfigFile();
        
    }

    public KeyCapture(int keyEndRun, int keyTrigger) {
        this.keyEndRun = keyEndRun;
        this.keyTrigger = keyTrigger;
        this.enterKey = this.keyTrigger;
        this.isWritting = false;
        this.endWritting = false;
        this.robotWritting = false;
        this.dataCaptured = "";
        this.tmpData = "";
        this.commandList.add("-tr");
        this.commandList.add("/tr");
        this.writeWithRobot = true;
        writeConfigFile();

    }

    public GoogleTranslator getTranslator() {
        return translator;
    }

    public void setTranslator(GoogleTranslator translator) {
        this.translator = translator;
    }

    public String getTranslated() {
        return translatedText;
    }

    public void setTranslated(String text) {
        this.translatedText = text;
    }

    public String getDataCaptured() {
        return dataCaptured.trim();
    }

    public String getDataTranslated() {
        return this.tmpData;
    }

    /**
     * Parsing the data obtained putting all the "valid" chars into the
     * dataCaptured
     *
     * @param key keycode value of the char
     * @param charString char in String format
     */
    private void setDataCaptured(int key, String charString) {
        if (!charString.trim().equals("")) {//while writting, put the key captured into the string
            this.dataCaptured += charString;
            System.out.println("capturing=" + this.dataCaptured);
        } else if (key == 8 && this.dataCaptured.length() > 0) {//delete key
            this.dataCaptured = this.dataCaptured.substring(0, this.dataCaptured.length() - 1);
        } else if (key == 32) {//space bar
            dataCaptured += " ";
        }
    }

    public int getKeyEndRun() {
        return keyEndRun;
    }

    public void setKeyEndRun(int keyEndRun) {
        this.keyEndRun = keyEndRun;
         if(keyEndRun==10)   this.keyEndRun=13;
    }

    public int getKeyTrigger() {
        return keyTrigger;
    }

    public void setKeyTrigger(int keyTrigger) {
        this.enterKey = keyTrigger;
        this.keyTrigger = keyTrigger;
        if(keyTrigger==10)   this.keyTrigger=13;

    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    private void setWritting(boolean isWritting) {
        this.isWritting = isWritting;
    }

    public boolean isWritting() {
        return this.isWritting;
    }

    private void setEndWritting(boolean endWritting) {
        this.endWritting = endWritting;
    }

    public boolean isEndWritting() {
        return this.endWritting;
    }
    /**
     * Open a File who have the config file of keyCapture
     * @param filename  file name of the configuration (default: config.txt)
     * @return String of the content in the file
     */
    public String readFile(FileReader filename, boolean search) {
            String result = "";
            String enter = System.getProperty("line.separator");
            try {
                BufferedReader br = new BufferedReader(filename);
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();
                while (line != null) {
                    sb.append(line + enter);
                    line = br.readLine();
                    if(search && line  != null){
                        setValueToVar(line);
                    }
                }
            result = sb.toString();
        } catch(IOException e) {
        }
        return result;
    }

    /**
     * Read the string and if it start with a name of a variable of the class, search if this line have a value.
     * Then set this value to the variable in the class
     */
    private void setValueToVar(String line){
        line  = line.trim();
        for( Field field : this.getClass().getDeclaredFields()){
            String name = field.getName();
            if(line.startsWith(name)){
                //convert to the type of variable
                String classType = field.getType().getSimpleName();
                //parsing values
                String value = line.substring(line.indexOf("=")+1, line.length() ).replace('\'',' ').trim();
                if(classType.equals("ArrayList")){
                    value = value.substring(2,value.length()-1);
                    ArrayList<String> arr = new ArrayList<>();
                    for(String str : value.split(",")){
                            arr.add(str.trim());
                    }
                    System.out.println("changed");
                    this.commandList = arr.size() > 0? arr : this.commandList;
                }
                    //arr.forEach((e)-> System.out.println(e));
                 if(!value.isEmpty()){
                     //release value of variables
                     switch (name.toLowerCase()){
                        case "keyendrun":
                                this.keyEndRun = Integer.parseInt(value);
                            break;
                        case "keytrigger":
                                this.keyTrigger = Integer.parseInt(value);
                            break;
                        case "writewithrobot":
                                this.writeWithRobot = Boolean.getBoolean(value);
                            break;
                        case "enterkey":
                                 this.enterKey = Integer.parseInt(value);
                            break;
                        case "sourcelang":
                             this.translator.setSourceLang(value);
                        break;
                        case "targetlang":
                            this.translator.setTargetLang(value);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Function to create a File with name"config.txt" and put in the configuration of the keycapture.
     * If the File exist, check if have variables writted and set the new configuration
     * @return File with content
     */
    public  File writeConfigFile(){
        //check  if the file exist in the current path
        String enter = System.getProperty("line.separator");
        String fileName = "config";
        File configFile = new File(fileName + ".txt");
        String txtToPut = 
                title + enter +
                "#keyCode of the key, that need to stop the execution " + enter +
                "keyEndRun = " + this.keyEndRun + enter + enter +
                "#keyCode of the key, that need to start the capture of the text to translate" + enter +
                "keyTrigger = " + this.keyTrigger + enter  + enter +
                "#commandList : commands to listener when the capture is started. This commands" + enter +
                "#are needed to know what text to translate after a capture. Example: /tr translate this" + enter +
                "commandList = ['/tr','-tr']"  + enter  + enter +
                "#boolean to know if you want to paste the translations after translated" + enter +
                "writeWithRobot =" + this.writeWithRobot + enter  + enter +
                "#key to enter into the interface that you need to paste the text(by default" + enter + 
                "#its same as keyTrigger). example: some special key binded" + enter +
                "enterKey =" + this.enterKey + enter +
                "#define the language that you write "+ enter +
                "sourceLang ="  +this.translator.getSourceLang() + enter +
                "#define the language that you want to translate/paste "+ enter +
                "targetLang ="  +this.translator.getTargetLang() + enter  ;
        if(configFile.exists()){
            try {
                //if exist go to read and setup the new config
                FileReader fr = new FileReader(configFile);
                String configWrited = readFile(fr,false);
                if(configWrited.startsWith(title)){
                    System.out.println("the file exist");
                    FileReader file = new FileReader(configFile);
                    configWrited = readFile(file,true);
                   
                    return configFile;
                }
                else{
                    System.out.println("maked new one");
                    return makeFile(fileName + "-1",txtToPut);//TODO release a better way to set the number
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            System.out.println("first make");
            return makeFile(fileName,txtToPut);
        }
       
        return null;
    }
    /**
     * make a file whit a name and put a content into it
     * @param name String of file, without the extention. by default is ´txt´
     * @param textToWrite String content to put in
     * @return File created
     */
    private File makeFile(String name , String textToWrite){
        try {
            FileWriter fw = new FileWriter(name + ".txt");
            PrintWriter pw = new PrintWriter(fw);
            pw.write(textToWrite);
            pw.close();
            return  new File(name + ".txt");
        } catch (IOException ex) {
            Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
        }
       return null;
    }

    /**
     * get the current clipboard, save it, and set the new clipboard with the
     * "msg" and paste it . Finally restore the old clipboard
     *
     * @param msg string to paste
     */
    public void pasteMsg(String msg) {
        //save current copy
        if (msg.equals("")) {
            return;
        }
        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection current = null;
        try {
            current = new StringSelection(clip.getData(DataFlavor.stringFlavor).toString());
        } catch (UnsupportedFlavorException | IOException ex) {
            Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
        }
        //set the new copy with the translate text        
        StringSelection translated = new StringSelection(msg);
        clip.setContents(translated, translated);
        //paste it
        Robot robot;
        try {
            robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.delay(200);
            robot.keyPress(KeyEvent.VK_V);
            robot.delay(200);
            robot.keyRelease(KeyEvent.VK_V);
            robot.delay(200);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (AWTException ex) {
            Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
        }
        //restore old copy       
        clip.setContents(current, current);
    }

    /**
     * execute the Robot class to type all the chars that are in the string
     *
     * @param msg String to write
     */
    public void runRobotWriteMsg(String msg) {
        System.out.println("msg=" + msg);
        Robot robot;
        try {
            robotWritting = true;
            robot = new Robot();
            robot.keyPress(this.enterKey);
            robot.keyRelease(this.enterKey);
            //print or write all the keys
            pasteMsg(msg);
            robot.delay(100);
            robot.keyPress(this.enterKey);
            robot.keyRelease(this.enterKey);
            robot.delay(400);
            robotWritting = false;
        } catch (AWTException ex) {
            Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String parsingQuery(String msg) {
        int indexOfCmd = ut.foundCmd(tmpData.toLowerCase(), commandList);
        //if have a command at end of string, cut it, else write until the end
        int len = indexOfCmd > msg.length() / 2 ? msg.substring(0, indexOfCmd - 1).length() : msg.length();
        int start = indexOfCmd > msg.length() / 2 ? 0 : 4;
        return msg.substring(start, len);
    }

    @Override
    public void run() {
        GlobalKeyboardHook keyboardHook = new GlobalKeyboardHook(true);//true for raw capture
        //create new thread of the translator
        Thread t = new Thread("Translator") {
            @Override
            public void run() {

                while (isRun()) {
                    if (isEndWritting() && !isWritting()) {
                        int indexOfCmd = ut.foundCmd(tmpData.toLowerCase(), commandList);
                        if (tmpData.length() > 0 && indexOfCmd >= 0) {
                            if (ut.foundParam(tmpData, paramList, commandList)) {
                                //searching for params to aplly into the translate
                            }
                            long initialTime = System.currentTimeMillis();
                            System.out.println("tmpdata=" + tmpData);
                            translatedText = getTranslator().translate(parsingQuery(tmpData) ,false);
                            //initiate robot and pausing KeyListener execution too
                            if (writeWithRobot) {
                                runRobotWriteMsg(translatedText);
                            }
                            long end = System.currentTimeMillis();
                            //testing time of execution
                            System.out.println("start=" + initialTime + " ; end= " + end + " ; total=" + (end - initialTime));
                            tmpData = "";
                        }
                    }
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(KeyCapture.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }

        };
        t.setPriority(10);
        t.start();
        setRun(true);
        System.out.println("Global keyboard hook successfully started, press [" + (char) getKeyEndRun()
                + "] key to shutdown. Connected keyboards:");
        keyboardHook.addKeyListener(new GlobalKeyAdapter() {
            String cS = "";

            @Override
            public void keyPressed(GlobalKeyEvent event) {
                if (!robotWritting) {//don't need to capture while robot is typing the translate text
                    int key = event.getVirtualKeyCode();
                    char c = event.getKeyChar();
                    cS = String.valueOf(c);
                    //leave the execution
                    if (key == getKeyEndRun() && !event.isControlPressed()  && !event.isShiftPressed()) {
                        setRun(false);
                    }
                    //activating capture
                    if (key == getKeyTrigger()) {
                        if (!isWritting()) {//first enter
                            System.out.println("fist enter");
                            setWritting(true);
                            setEndWritting(false);
                            return;
                        } else {//second enter
                            System.out.println("second enter");
                            setEndWritting(true);
                            if (getDataCaptured().length() > 0 && ut.foundCmd(getDataCaptured().toLowerCase(), commandList) >= 0) {
                                tmpData = dataCaptured;
                            }
                            dataCaptured = "";
                        }
                    }
                    if (isWritting() && !isEndWritting()) {//while is writting, capture keys
                        //special characters, changed  in keyPressed
                        if (key == 50 && event.isShiftPressed()) {//50 =[2]
                            cS = String.valueOf((char) 39);//capturing ["] and converting into [']
                        }
                        if (key == 55 && event.isShiftPressed()) {//55 = [7]
                            cS = String.valueOf('/');//know the value of shift +7
                        }
                        setDataCaptured(key, cS);
                        return;
                    } else {//normal clicking keys
                        setWritting(false);
                    }
                } else {//resetting capture variables
                    dataCaptured = "";
                    isWritting = false;
                    endWritting = false;
                }
            }
        });

        try {
            while (isRun()) {
                Thread.sleep(200);
            }
        } catch (InterruptedException e) {
            /* nothing to do here */ } finally {
            keyboardHook.shutdownHook();
            System.out.println("end keyCapture");
        }
    }
}
