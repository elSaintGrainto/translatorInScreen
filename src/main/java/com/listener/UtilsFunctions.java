/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.listener;

import com.sun.javafx.iio.ImageStorage;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import static sun.misc.ThreadGroupUtils.getRootThreadGroup;

/**
 *
 * @author juan
 */
public class UtilsFunctions {
    public final int LIMIT_TEXT = 40;
    public final Color Even= new Color(90, 123, 170);//par(spanish) order
    public final Color Odd= new Color(76, 158, 126);//impar(spanish) order
    
    
    /**
     * know the IP External
     * @return a external IP of your pc
     */
    public String getIp() {
        URL whatismyip = null;
        try {
            whatismyip = new URL("http://checkip.amazonaws.com");
        } catch (MalformedURLException ex) {
           Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        }catch(MalformedURLException  e){
            Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        return "";
    }

    /**
     * Obtain a zoomed image of the original
     * @param image Buffered Image to apply the zoom
     * @param zoom scale of zoom 
     * @return the new image
     */
    public BufferedImage getResizedImage(BufferedImage image,int zoom){
        int width = image.getWidth() * zoom;
        int height = image.getHeight() * zoom;
        
        BufferedImage img =new BufferedImage(width,height, image.getType());
        Graphics2D g = img.createGraphics();
        g.drawImage(image, 0, 0, width ,height , null);
        g.dispose();
        return img;
    }
    /**
     * Return the ImageIcon converted to BufferedImage
     * @param icon Icon of some component
     * @return BufferedImage from Icon
     */
    public BufferedImage getBufferedImageFromIcon(Icon icon){
        BufferedImage bi = new BufferedImage(
            icon.getIconWidth(),
            icon.getIconHeight(),
            BufferedImage.TYPE_INT_ARGB);
        java.awt.Graphics g = bi.createGraphics();
        icon.paintIcon(null, g, 0,0);
        g.dispose();
        return bi;
    }
    /**
     * Move a component in the scrollPane, but  in a section of it, this component don't stay static in the scroolpane
     * and get the BufferedImage  zoomed  and contained in the section of the scroolPane where the margin is
     * @param img BufferedImage contained in the scroolPane
     * @param source  ScroolPane catainer of the image
     * @param margin    JComponent to move in the scroolPane
     * @param section   Rectangle that of the image to cut
     * @param scaleZoom  int scale of zoom to apply in the image cutted
     * @return  BufferedImage of img zoomed and cutted
     */
    public BufferedImage getSectionResizedFromScrollPane(BufferedImage img, JScrollPane source, JComponent margin ,Rectangle section,int scaleZoom){
            //calculate the X and Y of the panelZoom position and move to into the scrollpane
            int extraX = source.getHorizontalScrollBar().getValue();
            int extraY = source.getVerticalScrollBar().getValue();
            int scrX = section.x + source.getLocation().x - margin.getWidth()/2 - extraX;
            int scrY = section.y + source.getLocation().y - margin.getHeight()/2 -extraY;
            margin.setLocation(new Point(scrX,scrY));
            //calculating the raster of the subimage
            return getImageZoomFromSection(img, section,scaleZoom);
    }
     /**
     * function to cut an image with a rectangle inBounds, and then return this section with a zoom
     * @param img BufferedImage to apply the zoom  and cut a section
     * @param section Bounds of a section to cut 
     * @param zoom scale to appy
     * @return BufferedImage resized of the secection
     */
    public  BufferedImage getImageZoomFromSection(BufferedImage img,Rectangle section,int zoom){
        //calculating the raster of the subimage
        int w = section.width <= img.getWidth()? section.width: img.getWidth();
        int h = section.height <= img.getHeight()? section.height: img.getHeight();
        int x = ((section.x - w/2) + w) > img.getWidth() ? img.getWidth() -w : (section.x - w/2);
        int y = ((section.y - h/2) + h) > img.getHeight() ? img.getHeight()- h : (section.y - h/2);
        x = x > 0 ? x :0;
        y = y > 0 ? y :0;
        return getResizedImage(img.getSubimage(x,y, w,h),4) ;
    }
    
    public int[][] getPixelsOfImg(BufferedImage image) {

      final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
      final boolean hasAlphaChannel = image.getAlphaRaster() != null;

      int[][] result = new int[image.getHeight()][image.getWidth()];
      if (hasAlphaChannel) {
         final int pixelLength = 4;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
            argb += ((int) pixels[pixel + 1] & 0xff); // blue
            argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == image.getWidth()) {
               col = 0;
               row++;
            }
         }
      } else {
         final int pixelLength = 3;
         for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
            int argb = 0;
            argb += -16777216; // 255 alpha
            argb += ((int) pixels[pixel] & 0xff); // blue
            argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
            argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red
            result[row][col] = argb;
            col++;
            if (col == image.getWidth()) {
               col = 0;
               row++;
            }
         }
      }
      return result;
   }
    /**
     * change the Font of a component
     * @param comp JComponent to change his Font
     * @param path  String path where the " font.ttf " is
     * @param size  int size of the Font
     * @param type type of the Font (use Font.something)
     */
    public void changeFont(final JComponent comp, String path,int size,int type){
        Font fuente = null;
        InputStream  inputFont = null;
        try {
            inputFont = new BufferedInputStream(new FileInputStream(path));
            fuente = Font.createFont(Font.TRUETYPE_FONT, inputFont);
            fuente = fuente.deriveFont(type, size);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FontFormatException | IOException ex) {
            Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        ge.registerFont(fuente);
        comp.setFont(fuente);
    }
    /**
     * Take a screeshoot with margins or without, and save it in to a file
     * @param name  name of the screenshoot, to save in to file.
     * @param seconds_to_wait   seconds to take a capture
     * @param rect  Rectangle for limit the screenshoot
     * @return  a BufferedImage of screenshoot
     * @throws AWTException
     * @throws IOException
     * @throws InterruptedException 
     */
    public BufferedImage screenshoot(String name,int seconds_to_wait,Rectangle rect) throws AWTException, IOException, InterruptedException{
        if(rect==null){
            rect=new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        }
        //capturing image
        long sec = (long) (seconds_to_wait * 1000) ;//timer to capture screen
        Thread.sleep(sec+250);
        BufferedImage capture = new Robot().createScreenCapture(rect);
        //save Jpg
        if(!name.equals("")){//if not empty, save into a file
            String extention="jpg";
            if(!NameHaveExtention(name)){
                name+="."+extention;
            } 
            //else it will changed by the name of the file
            File file =new File(name);
            ImageIO.write(capture, extention, file);
            capture = ImageIO.read(file);
        }
        return capture;
    }
    /**
     * function to recognize if the string have an extention in, like "example.jpg".
     * If it have the extention in her name, gonna be her extention in the file, else
     * the default extention is JPG
     * Seek for image formats
     * @param name String to search
     * @return  true if found an image format
     */
    public boolean NameHaveExtention(String name){
         //recognize if the name have an extention
        String[] arr_name=name.split("\\."); //use \\ to escape the dot, because the REGEX
        String[] formats = ImageIO.getReaderFormatNames();
        for (String format : formats) {
            if (format.equals(arr_name[arr_name.length-1])) {//search at the end
                return true;
            }
        }
        return false;
    }   
    /** 
     * Search into the string if it have a command, and a param after this
     * example -tr -p
     * @param src String to search inside of it
     * @param listParam List of params of some command preDefined
     * @param lCmd List of Commands predefined
     * @return true if have a parameter aftera command
     */
    public boolean foundParam(String src,List listParam, List lCmd){
        if(listParam.isEmpty() || src.length() == 0 || lCmd.isEmpty())return false;
        else{
            for (int i = 0; i < listParam.size(); i++) {
                //search the param into the String
                int pos = src.indexOf(listParam.get(i).toString());
                //if found a param after a command, need min and max
                if(pos >= getMinSizeIn(lCmd) && pos <= getMaxSizeIn(lCmd) ) return true;
            }
        }
        return false;
    }
    /**
     * Search a list of commmands in the String, the must be at begin
     * but could be at end of string too
     * @param src String to search inside of it
     * @param list List of commands to search
     * @return the position of a command if it's at begining or at end, else return -1
     */
    public int foundCmd(String src,List list){
       if(list.isEmpty() || src.length() == 0)return -1;
       for (int i = 0; i < list.size(); i++) {
           //searching a cmd in the string
           int pos = src.indexOf(list.get(i).toString());
           int sizeCmd = list.get(i).toString().length();
           //if found a command at start or at end
           if(pos == 0 || pos == (src.length() - sizeCmd)){
               return pos;
           }
       }
       return -1;
   }
    /**
     * Compare all the params into the List an obtain the max length of a param
     * @param list List to be searched
     * @return max length that are in the List
     */
    public int getMaxSizeIn(List list){
        if(list.isEmpty())return -1;
        int max=0;
        for (int i = 0; i < list.size(); i++) {
            int size = (int) list.get(i).toString().length();
            if(size > max) max = size;            
        }
        return max;
    }
    /**
     * Compare all the params into the List an obtain the min length of a param
     * @param list List to be searched
     * @return min length that are in the List
     */
    public int getMinSizeIn(List list){
        if(list.isEmpty())return -1;
        int min= 99999999;
        for (int i = 0; i < list.size(); i++) {
            int size = (int) list.get(i).toString().length();
            if(size < min) min = size;
        }
        return min;
    }
    /**
     * Obtain all the threads that are currently running
     * @return Array of all the Threads founded
     */
    public Thread[] getAllThreads( ) {
        final ThreadGroup root = getRootThreadGroup( );
        final ThreadMXBean thbean = ManagementFactory.getThreadMXBean( );
        int nAlloc = thbean.getThreadCount( );
        int n = 0;
        Thread[] threads;
        do {
            nAlloc *= 2;
            threads = new Thread[ nAlloc ];
            n = root.enumerate( threads, true );
        } while ( n == nAlloc );
        return java.util.Arrays.copyOf( threads, n );
    }
    /**
     * Add a JLabel into the seleted JPanel, and give him a Color background 
     * depend if it's Even or Odd.
     * the Height of JLabel is calculated by 25*(text.length/LIMIT_TEXT) by default
     * 
     * @param panel JPanel to put the JLabel
     * @param text String to put into the JLabel
     */
    public void addLabel(JPanel panel,String text){
        int cant = panel.getComponents().length;
        int w = panel.getWidth();
        int h = 25* ((int)(text.length()/LIMIT_TEXT)+1);
        int y = 5;
        if (cant == 0) 
            y = 5;
        else
            y += (int)panel.getComponent(cant-1).getBounds().getY()+h;
        Rectangle rect= new Rectangle(0,y,w,h);
        JLabel tmp = new JLabel(""+cant+"-  "+text);
        if((cant % 2) == 0)tmp.setBackground(Even);
        else tmp.setBackground(Odd);
        tmp.setBounds(rect);
        tmp.setVisible(true);
        tmp.setOpaque(true);
        panel.add(tmp);
        panel.validate();
        panel.repaint();
    }
    /**
     * Add a JLabel into the seleted JPanel, and give him a Color background 
     * depend if it's Even or Odd.
     * the Height of JLabel is calculated by 25*(text.length/LIMIT_TEXT) by default
     * the Width of JLabel is inherit of panel
     * @param panel JPanel to put the JLabel
     * @param text String to put into the JLabel
     */
    public void addLabel(JPanel panel,String text,int heightPerLine){
        int cant = panel.getComponents().length;
        int w = panel.getWidth();
        int h = heightPerLine* ((int)(text.length()/LIMIT_TEXT)+1);
        int y = 5;
        if (cant == 0) 
            y = 5;
        else
            y += (int)panel.getComponent(cant-1).getBounds().getY()+h;
        Rectangle rect= new Rectangle(0,y,w,h);
        JLabel tmp = new JLabel(""+cant+"-  "+text);
        if((cant % 2) == 0)tmp.setBackground(Even);
        else tmp.setBackground(Odd);
        tmp.setBounds(rect);
        tmp.setVisible(true);
        tmp.setOpaque(true);
        panel.add(tmp);
        panel.validate();
        panel.repaint();
    }
    /**
     * Make a String with html tags, with multilines ('br' tag) depending of limit
     * 
     * @param text text to convert into miultilines
     * @param limit int quantity of characteres per line
     * @return text with multiline and html tags
     */
    public String formatTextLog(String text, int limit){
        int len = text.length();
        if(len <=limit){
            return text;
        }
        int cantOfEnters= len/limit;//cant of enters to put into the string
        String tmp="";
        for (int i = 0; i < cantOfEnters+1; i++) {
            int calc=limit*(i+1);
            if(calc<=len)
                tmp+= text.substring(limit*i,calc)+"<br>";
            else//final of chain
                tmp+=text.substring(limit*i,len);
        }
        return "<html><center>"+tmp+"</center></html>";
    }
    /**
     * make a Thread that move the Component to the Y axis of a Rectangle
     * go to UP of the Y axis
     * @param point Point of a position
     * @param cmpt component to move to the point positio 
     * @param visible switch the visivility of the component after of the move
     */
    public void moveAnimationUp(final Point point, final JComponent cmpt,final boolean visible){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int step=10;
               int yK = cmpt.getBounds().y;//save current Y axis of button
               while(yK > point.y) {//move up
                   yK-=step;
                   cmpt.setLocation(cmpt.getBounds().x,yK );
                   try {
                        Thread.sleep(10);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
    /**
     * make a Thread that move the Component to the Y axis of a Rectangle
     * go to UP of the Y axis
     * @param point point of a position
     * @param cmpt component to move to the point position 
     * @param visible switch the visivility of the component after of the move
     * @param step pixels to move per tick
     */
   public void moveAnimationUp(final Point point, final JComponent cmpt,final boolean visible,final int step){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int yK = cmpt.getBounds().y;//save current Y axis of button
               while(yK > point.y) {//move up
                   yK-=step;
                   cmpt.setLocation(cmpt.getBounds().x,yK );
                   try {
                        Thread.sleep(10);
                   } catch (InterruptedException ex) {
                      Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
   /**
    * make a Thread that move the Component to the Y axis of a Rectangle
    * go to UP of the Y axis
    * @param point point of a position
    * @param cmpt component to move to the point position 
     * @param visible switch the visivility of the component after of the move
    * @param step pixels to move per tick
    * @param timePerTick miliseconds to wait each time to do the move
    */
    public void moveAnimationUp(final Point point, final JComponent cmpt,final boolean visible, final int step,final long timePerTick){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int yK = cmpt.getBounds().y;//save current Y axis of button
               while(yK > point.y) {//move up
                   yK -= step;
                   cmpt.setLocation(cmpt.getBounds().x,yK );
                   try {
                        Thread.sleep(timePerTick);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
    /**
     * make a Thread that move the Component to the Y axis of a Rectangle
     * go to DOWN of the Y axis
     * @param point point of a position
     * @param cmpt component to move to the point position 
     * @param visible switch the visivility of the component after of the move
     */
    public void moveAnimationDown(final Point point, final JComponent cmpt,final boolean visible){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int step=5;
               int yK = cmpt.getBounds().y;//save current Y axis of button
               while(yK < point.y) {//move down
                   yK+=step;
                   cmpt.setLocation(point.x,yK );
                   try {
                        Thread.sleep(10);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
 /**
     * make a Thread that move the Component to the Y axis of a Rectangle
     * go to DOWN of the Y axis
     * @param point point of a position
     * @param cmpt component to move to the point position 
     * @param visible switch the visivility of the component after of the move
     * @param step pixels to move per tick
     */
    public void moveAnimationDown(final Point point, final JComponent cmpt,final boolean visible,final int step){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int yK = cmpt.getBounds().y;//save current Y axis of button
               System.out.println("yK="+yK+"\n rect="+point.y);
               while(yK < point.y) {//move down
                   yK+=step;
                   cmpt.setLocation(point.x,yK );
                   try {
                        Thread.sleep(10);
                   } catch (InterruptedException ex) {
                      Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
    /**
     * make a Thread that move the Component to the Y axis of a Rectangle
     * go to DOWN of the Y axis
     * @param point point of a position
     * @param cmpt component to move to the point position 
     * @param visible switch the visivility of the component after of the move
     * @param step  pixels to move per tick
     * @param timePerTick miliseconds to wait each time to do the move
     */
    public void moveAnimationDown(final Point point, final JComponent cmpt,final boolean visible,final int step,final long timePerTick){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int yK = cmpt.getBounds().y;//save current Y axis of button
               System.out.println("yK="+yK+"\n rect="+point.y);
               while(yK < point.y ) {//move down
                   yK+=step;
                   cmpt.setLocation(point.x,yK );
                   try {
                        Thread.sleep(timePerTick);
                   } catch (InterruptedException ex) {
                      Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               cmpt.revalidate();
           }
       };
        t.setPriority(10);
        t.start();
    }
    public void moveAnimationSide(final Point point, final JComponent cmpt, final String side , final boolean visible,final int step,final long timePerTick){
        Thread t= new Thread("moveConfig"){
           @Override
           public void run(){
               int xK = cmpt.getBounds().x;//save current Y axis of button
               while(xK < point.x && side.toUpperCase().equals("RIGHT")) {//move down
                   xK+=step;
                   cmpt.setLocation(xK,point.y);
                   try {
                        Thread.sleep(timePerTick);
                   } catch (InterruptedException ex) {
                      Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               while(xK > point.x && side.toUpperCase().equals("LEFT")) {//move down
                   xK-=step;
                   cmpt.setLocation(xK,point.y);
                   try {
                        Thread.sleep(timePerTick);
                   } catch (InterruptedException ex) {
                      Logger.getLogger(UtilsFunctions.class.getName()).log(Level.SEVERE, null, ex);
                   }
               }
               cmpt.setVisible(visible);
               
               cmpt.revalidate();
               cmpt.setLocation(point.x,point.y);
           }
       };
        t.setPriority(10);
        t.start();
    }
    /**
     * chage the cursor in each margin of the component, and if you want to move too
     * you can move the component in each margin
     * recomendation: use this function in mousedragged event 
     * @param component JComponent do you want to shw the arrows and/or move
     * @param evt   MouseEvent  from de mouselistener
     * @param move  boolean to move the margin or not
     * @return  resized Rectangle of the component
     */
   public Rectangle resizeComponentWithMouse(JComponent component, java.awt.event.MouseEvent evt, boolean move){
        Rectangle rectComp = component.getBounds();
        Point pointEvt = new Point(evt.getXOnScreen(),evt.getYOnScreen());
        int extraY = 10, extraX =20;
        int yHeight = rectComp.y + rectComp.height;
        int xWidth = rectComp.x + rectComp.width;
        
        if((rectComp.y + extraY) >= pointEvt.y && (rectComp.y - extraY) <= pointEvt.y &&
           (rectComp.x + (extraY * 2)) < pointEvt.x && (xWidth - (extraY* 2) > pointEvt.x ) ){
            //calculating Top margin of object
            component.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
            if(move && rectComp.height >=10){
                rectComp.height  -= (pointEvt.y -rectComp.y);
                rectComp.y = pointEvt.y;
                rectComp.height= rectComp.height <10? 10: rectComp.height;
            }
        }else if((yHeight + extraY) >= pointEvt.y  &&(yHeight - extraY) <= pointEvt.y &&
                (rectComp.x + (extraY * 2)) < pointEvt.x && (xWidth - (extraY * 2) > pointEvt.x )){
            //calculating Bottom margin of object
            component.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
            if(move && rectComp.height >=10 && evt.getY() >10 )rectComp.height = evt.getY();
        }else if((xWidth + extraX) >= pointEvt.x && (xWidth - extraX) <= pointEvt.x){
            //calculating Right margin of object
           component.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
            if(move && rectComp.width >10 && evt.getX() >=10 )rectComp.width = evt.getX();
        }else if((rectComp.x + extraX) >= pointEvt.x && (rectComp.x -extraX) <= pointEvt.x){
            //calculating Left margin of object
            component.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
            if(move && rectComp.width >=10 ){
                rectComp.width -= (pointEvt.x  - rectComp.x) ;
                rectComp.x = pointEvt.x;
            }
        }else{
            component.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }    
       return rectComp;
    }
   /**
     * chage the cursor in each margin of the component, and if you want to move too
     * you can move the component in each margin
     * recomendation: use this function in mousedragged event 
     * @param component JComponent do you want to shw the arrows and/or move
     * @param evt   MouseEvent  from de mouselistener
     * @param move  boolean to move the margin or not
     * @param desactivateMargin String ArrayList of margins do you want to desactivate in the resize. USE UPPERCASE
     * @return  resized Rectangle of the component
     */
   public Rectangle resizeComponentWithMouse(JComponent component, java.awt.event.MouseEvent evt, boolean move, ArrayList<String> desactivateMargin){
        Rectangle rectComp = component.getBounds();
        Point pointEvt = new Point(evt.getXOnScreen(),evt.getYOnScreen());
        int extraY = 7, extraX =15;
        int yHeight = rectComp.y + rectComp.height;
        int xWidth = rectComp.x + rectComp.width;        

        if((rectComp.y + extraY) >= pointEvt.y && (rectComp.y - extraY) <= pointEvt.y &&
           (rectComp.x + (extraY * 2)) < pointEvt.x && (xWidth - (extraY* 2) > pointEvt.x ) ){
            //calculating Top margin of object
            if(!desactivateMargin.contains("TOP")){
                component.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
                if(move && rectComp.height >=10){
                    rectComp.height  -= (pointEvt.y -rectComp.y);
                    rectComp.y = pointEvt.y;
                    rectComp.height= rectComp.height <10? 10: rectComp.height;
                }
            }
        }else if((yHeight + extraY) >= pointEvt.y  &&(yHeight - extraY) <= pointEvt.y &&
                (rectComp.x + (extraY * 2)) < pointEvt.x && (xWidth - (extraY * 2) > pointEvt.x )){
            //calculating Bottom margin of object
            if(!desactivateMargin.contains("BOTTOM")){
                component.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
                if(move  && rectComp.height >=10 && evt.getY() >10 )rectComp.height = evt.getY();
            }
        }else if((xWidth + extraX) >= pointEvt.x && (xWidth - extraX) <= pointEvt.x){
            //calculating Right margin of object
            if(!desactivateMargin.contains("RIGHT")){
                component.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
                if(move && rectComp.width >10 && evt.getX() >=10)rectComp.width = evt.getX();
            }
        }else if((rectComp.x + extraX) >= pointEvt.x && (rectComp.x -extraX) <= pointEvt.x){
            //calculating Left margin of object
            if(!desactivateMargin.contains("LEFT")){
                component.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
                if(move  && rectComp.width >=10){
                    rectComp.width -= (pointEvt.x  - rectComp.x) ;
                    rectComp.x = pointEvt.x;
                }
            }   
        }else{
            component.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }    
       return rectComp;
    }
    /**
     * check if the MouseEvent is in the "middle" opsition of the component
     * the middle is seted by the  50% of it
     * @param component JComponent to obtein his middle
     * @param pos   Point of the position of "MousePress event"
     * @param evt    MouseEvent of MouseDragged
     * @return  true if the evt is in the middle of the object
     */
    public boolean isInMiddleBounds(JComponent component , Point pos, java.awt.event.MouseEvent evt )   {
        Rectangle rect = component.getBounds();
       if( !(evt.getX() - pos.x <= (rect.width * 0.7) ) &&  !(evt.getX() - pos.x >= (rect.width * 0.3))) return false;
       if( !(evt.getY() - pos.y <= (rect.height * 0.7) ) &&  !(evt.getY() -  pos.y >= (rect.height * 0.3))) return false;
       return true;
    }
    /**
     * Move the component whit the MouseDragged event
     * @param component JComponent  to move 
     * @param evt MouseEvent of  the MouseLstener
     * @param initPosition Point position of the MousePressed
     */
   public void MoveComponentWithMouse(JComponent component , java.awt.event.MouseEvent evt, Point initPosition ){
       component.setCursor(new Cursor(Cursor.MOVE_CURSOR));
       component.setLocation(new Point(component.getLocation().x  + evt.getX() - initPosition.x ,  component.getLocation().y  + evt.getY()  - initPosition.y));
       component.revalidate();
   }
   /**
     * Move the component whit the MouseDragged event
     * @param component JComponent  to move 
     * @param evt MouseEvent of  the MouseLstener
     * @param initPosition Point position of the MousePressed
     * @param XMove boolean to activate or not the X Axis moving
     * @param YMove boolean to activate or not the Y Axis moving
     */
   public void MoveComponentWithMouse(JComponent component , java.awt.event.MouseEvent evt, Point initPosition, boolean XMove,boolean YMove){
       Point p = new Point();
       p.x = XMove ? component.getLocation().x  + evt.getX() - initPosition.x : component.getBounds().x;
       p.y = YMove ? component.getLocation().y  + evt.getY()  - initPosition.y : component.getBounds().y;
       component.setCursor(new Cursor(Cursor.MOVE_CURSOR));
       component.setLocation(new Point(p));
       component.revalidate();
   }

   
}

 
