/*
 * The MIT License
 *
 * Copyright 2018 juan.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.listener;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author juan
 */
public class ListCustomRender implements ListCellRenderer{
    public ArrayList<Color> background = new ArrayList<>();
    
    private DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
    @Override
    public JComponent getListCellRendererComponent(JList list, Object value, int index,
        boolean isSelected, boolean cellHasFocus) {
        JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
            isSelected, cellHasFocus);
        MyIcon icon= new MyIcon();
        icon.index=index;
        renderer.setIcon(icon);
        renderer.setIconTextGap(10);
        renderer.setHorizontalAlignment(0);
        return renderer;
    }   
class MyIcon implements Icon {
    int index = 0;
    public MyIcon() {
    }
    
    public int getIconHeight() {
      return 25;
    }

    public int getIconWidth() {
      return 50;
    } 

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.setColor(background.get(index));
        g.fillRoundRect(8,5, 70,20,8,8);
    }
    }
}
