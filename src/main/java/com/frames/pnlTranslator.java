/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frames;

import com.translator.Translator;
import java.util.ArrayList;
import java.util.List;
import com.jm.translatorinscreen.GoogleTranslator;
import javax.swing.JOptionPane;
/**
 *
 * @author juan
 */
public class pnlTranslator extends javax.swing.JPanel {

    /**
     * Creates new form pnlTranslator
     */
    public String apikey = "";
    public boolean WriteTranslation = true;
    public String source="";
    public String target="";
    public List sourceList =new  ArrayList<>();
    public List targetList = new ArrayList<>();
    public Translator tra = new Translator("es","en");
    String[] langName;
    String[] langCode;
    public pnlTranslator() {
        initComponents();
        lblT.setText("en");
        lblS.setText("es");
        tra.setApikey("AIzaSyBlQA798Y252WItctghAPg1IbyEFB8gz7Q");
        this.lblMyIP.setText(frmMain.ut.getIp());
        langName = new String[] {"Afrikaans","Albanian","Amharic","Arabic","Armenian","Azerbaijani","Basque",
            "Belarusian","Bengali","Bosnian","Bulgarian","Catalan","Cebuano","Chichewa","Chinese (Simplified)","Chinese (Traditional)",
            "Corsican","Croatian","Czech","Danish","Dutch","English","Esperanto","Estonian","Filipino","Finnish","French",
            "Frisian", "Galician","Georgian","German","Greek","Gujarati","Haitian Creole","Hausa","Hawaiian","Hebrew",
            "Hindi","Hmong","Hungarian", "Icelandic","Igbo","Indonesian","Irish","Italian","Japanese","Javanese","Kannada",
            "Kazakh","Khmer","Korean","Kurdish (Kurmanji)","Kyrgyz","Lao","Latin","Latvian","Lithuanian","Luxembourgish",
            "Macedonian","Malagasy","Malay","Malayalam","Maltese","Maori","Marathi","Mongolian","Myanmar (Burmese)",
            "Nepali","Norwegian","Pashto","Persian","Polish","Portuguese","Punjabi","Romanian","Russian","Samoan",
            "Scots Gaelic", "Serbian","Sesotho","Shona","Sindhi","Sinhala","Slovak","Slovenian","Somali","Spanish","Sundanese",
            "Swahili","Swedish", "Tajik","Tamil","Telugu","Thai","Turkish","Ukrainian","Urdu","Uzbek","Vietnamese","Welsh",
            "Xhosa","Yiddish","Yoruba","Zulu"};
        langCode = new String []{"af","sq","am","ar","hy","az","eu","be","bn","bs","bg","ca","ceb","ny","zh","zh-TW","co",
          "hr","cs","da","nl","en","eo","et","tl","fi","fr","fy","gl","ka","de","el","gu","ht","ha","haw","iw","hi","hmn","hu","is","ig","id",
          "ga","it","ja","jw","kn","kk","km", "ko","ku","ky","lo","la","lv","lt","lb","mk","mg","ms","ml","mt","mi","mr","mn","my","ne",
          "no","ps","fa","pl","pt","pa","ro","ru","sm","gd","sr","st","sn","sd","si","sk","sl","so","es","su","sw","sv","tg","ta","te","th",
          "tr","uk","ur","uz","vi","cy","xh","yi","yo","zu"};
  
        cmbSource.addItem("select Language");
        cmbTarget.addItem("select Language");
        for (int i = 0; i < langName.length; i++) {
            cmbSource.addItem(langName[i]);
            cmbTarget.addItem(langName[i]);
            
        }
        cmbSource.setSelectedIndex(0);
        cmbTarget.setSelectedIndex(0);
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlLayer = new javax.swing.JLayeredPane();
        pnlFondo = new javax.swing.JPanel();
        txtApikey = new javax.swing.JTextField();
        lblApi = new javax.swing.JLabel();
        lblGeneral = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblMyIP = new javax.swing.JLabel();
        chkWriteTranslation = new javax.swing.JCheckBox();
        pnlDivisor = new javax.swing.JPanel();
        lblTitle = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblTarget = new javax.swing.JLabel();
        cmbTarget = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        lblSource = new javax.swing.JLabel();
        cmbSource = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtOrigin = new javax.swing.JTextField();
        txtDestiny = new javax.swing.JTextField();
        btnTranslate = new javax.swing.JButton();
        lblS = new javax.swing.JLabel();
        btnSwap = new javax.swing.JButton();
        lblT = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblAuthor = new javax.swing.JLabel();

        setBackground(new java.awt.Color(13, 37, 69));
        setPreferredSize(new java.awt.Dimension(651, 558));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlFondo.setBackground(new java.awt.Color(52, 71, 96));
        pnlFondo.setPreferredSize(new java.awt.Dimension(642, 558));
        pnlFondo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtApikey.setBackground(new java.awt.Color(231, 255, 255));
        txtApikey.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        txtApikey.setToolTipText("Put  a Api key of Google Translator here, if you want");
        txtApikey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtApikeyFocusLost(evt);
            }
        });
        txtApikey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApikeyActionPerformed(evt);
            }
        });
        pnlFondo.add(txtApikey, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 240, -1));

        lblApi.setBackground(new java.awt.Color(121, 140, 151));
        lblApi.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lblApi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblApi.setText("ApiKey Google:");
        lblApi.setOpaque(true);
        pnlFondo.add(lblApi, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 240, 40));

        lblGeneral.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        lblGeneral.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblGeneral.setText("Service general config:");
        pnlFondo.add(lblGeneral, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 44, 270, -1));

        jPanel3.setBackground(new java.awt.Color(121, 140, 151));
        jPanel3.setToolTipText("See your IP to check if it's in the white list of your api key");

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel1.setText("Your IP:");

        lblMyIP.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblMyIP, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                    .addComponent(lblMyIP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlFondo.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 240, -1));

        chkWriteTranslation.setBackground(new java.awt.Color(121, 140, 151));
        chkWriteTranslation.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        chkWriteTranslation.setSelected(true);
        chkWriteTranslation.setText("WriteTranslation:");
        chkWriteTranslation.setToolTipText("Select if you want to autowrite the translated text");
        chkWriteTranslation.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chkWriteTranslation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkWriteTranslationActionPerformed(evt);
            }
        });
        pnlFondo.add(chkWriteTranslation, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, 240, 40));

        pnlDivisor.setBackground(new java.awt.Color(58, 94, 104));
        pnlDivisor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTitle.setFont(new java.awt.Font("SansSerif", 0, 16)); // NOI18N
        lblTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitle.setText("Set Default Language Translation :");
        pnlDivisor.add(lblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 37, 290, 34));

        jPanel1.setBackground(new java.awt.Color(121, 140, 151));
        jPanel1.setToolTipText("Select the Language of the output text to translate");

        lblTarget.setBackground(new java.awt.Color(121, 140, 151));
        lblTarget.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lblTarget.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTarget.setText("Language target:");
        lblTarget.setOpaque(true);

        cmbTarget.setBackground(new java.awt.Color(55, 83, 100));
        cmbTarget.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        cmbTarget.setToolTipText("Select the language do you need to tranlate");
        cmbTarget.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTargetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(cmbTarget, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 24, Short.MAX_VALUE))
            .addComponent(lblTarget, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(lblTarget, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(cmbTarget, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlDivisor.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 250, 80));

        jPanel2.setBackground(new java.awt.Color(121, 140, 151));
        jPanel2.setToolTipText("Select the Language of the input text to translate");

        lblSource.setBackground(new java.awt.Color(121, 140, 151));
        lblSource.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lblSource.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSource.setText("Language source:");
        lblSource.setOpaque(true);

        cmbSource.setBackground(new java.awt.Color(55, 83, 100));
        cmbSource.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        cmbSource.setToolTipText("Select the language of the text captured");
        cmbSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSourceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSource, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(cmbSource, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(lblSource, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cmbSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlDivisor.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 110, 250, 80));

        pnlFondo.add(pnlDivisor, new org.netbeans.lib.awtextra.AbsoluteConstraints(276, 0, 300, 320));

        jPanel4.setBackground(new java.awt.Color(52, 81, 112));
        jPanel4.setLayout(null);

        jLabel3.setBackground(new java.awt.Color(52, 110, 159));
        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Test the translator:");
        jLabel3.setOpaque(true);
        jPanel4.add(jLabel3);
        jLabel3.setBounds(0, 0, 580, 30);

        txtOrigin.setBackground(new java.awt.Color(231, 255, 255));
        jPanel4.add(txtOrigin);
        txtOrigin.setBounds(70, 60, 190, 40);

        txtDestiny.setBackground(new java.awt.Color(231, 255, 255));
        jPanel4.add(txtDestiny);
        txtDestiny.setBounds(70, 130, 190, 40);

        btnTranslate.setBackground(new java.awt.Color(1, 123, 255));
        btnTranslate.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        btnTranslate.setText("Translate");
        btnTranslate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTranslateActionPerformed(evt);
            }
        });
        jPanel4.add(btnTranslate);
        btnTranslate.setBounds(410, 100, 130, 40);

        lblS.setBackground(new java.awt.Color(58, 81, 96));
        lblS.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lblS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblS.setOpaque(true);
        jPanel4.add(lblS);
        lblS.setBounds(300, 60, 70, 40);

        btnSwap.setBackground(new java.awt.Color(1, 123, 255));
        btnSwap.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/30x30/010-sort-3.png"))); // NOI18N
        btnSwap.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSwapActionPerformed(evt);
            }
        });
        jPanel4.add(btnSwap);
        btnSwap.setBounds(320, 100, 30, 30);

        lblT.setBackground(new java.awt.Color(58, 81, 96));
        lblT.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        lblT.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblT.setOpaque(true);
        jPanel4.add(lblT);
        lblT.setBounds(300, 130, 70, 40);

        jLabel4.setBackground(new java.awt.Color(58, 81, 96));
        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("From :");
        jLabel4.setOpaque(true);
        jPanel4.add(jLabel4);
        jLabel4.setBounds(20, 60, 50, 40);

        jLabel5.setBackground(new java.awt.Color(58, 81, 96));
        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("To :");
        jLabel5.setOpaque(true);
        jPanel4.add(jLabel5);
        jLabel5.setBounds(20, 130, 50, 40);

        lblAuthor.setBackground(new java.awt.Color(52, 81, 112));
        lblAuthor.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        lblAuthor.setForeground(new java.awt.Color(102, 255, 102));
        lblAuthor.setText("/elSaintGrainto");
        lblAuthor.setOpaque(true);
        jPanel4.add(lblAuthor);
        lblAuthor.setBounds(410, 180, 150, 30);

        pnlFondo.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 580, 220));

        pnlLayer.setLayer(pnlFondo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout pnlLayerLayout = new javax.swing.GroupLayout(pnlLayer);
        pnlLayer.setLayout(pnlLayerLayout);
        pnlLayerLayout.setHorizontalGroup(
            pnlLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLayerLayout.createSequentialGroup()
                .addComponent(pnlFondo, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlLayerLayout.setVerticalGroup(
            pnlLayerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlLayerLayout.createSequentialGroup()
                .addComponent(pnlFondo, javax.swing.GroupLayout.PREFERRED_SIZE, 545, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 22, Short.MAX_VALUE))
        );

        add(pnlLayer, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void txtApikeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtApikeyFocusLost
       //release apikey in the app
        String txt= txtApikey.getText();
        if(!txt.trim().equals(""))
            apikey= txt;
        
    }//GEN-LAST:event_txtApikeyFocusLost

    private void txtApikeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApikeyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApikeyActionPerformed

    private void chkWriteTranslationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkWriteTranslationActionPerformed
       WriteTranslation= chkWriteTranslation.isSelected();
    }//GEN-LAST:event_chkWriteTranslationActionPerformed

    private void cmbTargetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTargetActionPerformed
        int index= cmbTarget.getSelectedIndex()-1;
        String txt = cmbTarget.getSelectedItem().toString();
        target = txt.equals("select Language")? "" : langCode[index];
        System.out.println("target="+target);
        if(!target.equals(""))lblT.setText(target);
    }//GEN-LAST:event_cmbTargetActionPerformed

    private void cmbSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSourceActionPerformed
        int index = cmbSource.getSelectedIndex()-1;
        String txt = cmbSource.getSelectedItem().toString();
         System.out.println("index="+index+" ; ="+txt);
        source = txt.equals("select Language")? "" : langCode[index];
        System.out.println("source="+source);
        if(!source.equals(""))lblS.setText(source);
    }//GEN-LAST:event_cmbSourceActionPerformed

    private void btnTranslateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTranslateActionPerformed
        //button to test the translator and have a preview of it
        String src = lblS.getText();
        String tg = lblT.getText();
        //init the translator with params
        GoogleTranslator gt = new GoogleTranslator(src,tg);
        gt.setApikey("AIzaSyBlQA798Y252WItctghAPg1IbyEFB8gz7Q");
        String txt = txtOrigin.getText();
        if(!txt.trim().equals("")){
            String destiny = gt.translate(txt, false);//translating
            txtDestiny.setText(destiny);
        }else JOptionPane.showMessageDialog(null,"Need a text to translate");
    }//GEN-LAST:event_btnTranslateActionPerformed

    private void btnSwapActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSwapActionPerformed
        //Swap languages
        String src = lblS.getText();
        String tg = lblT.getText();
        lblT.setText(src);
        lblS.setText(tg);
    }//GEN-LAST:event_btnSwapActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSwap;
    private javax.swing.JButton btnTranslate;
    private javax.swing.JCheckBox chkWriteTranslation;
    private javax.swing.JComboBox<String> cmbSource;
    private javax.swing.JComboBox<String> cmbTarget;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel lblApi;
    private javax.swing.JLabel lblAuthor;
    private javax.swing.JLabel lblGeneral;
    private javax.swing.JLabel lblMyIP;
    private javax.swing.JLabel lblS;
    private javax.swing.JLabel lblSource;
    private javax.swing.JLabel lblT;
    private javax.swing.JLabel lblTarget;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JPanel pnlDivisor;
    private javax.swing.JPanel pnlFondo;
    private javax.swing.JLayeredPane pnlLayer;
    private javax.swing.JTextField txtApikey;
    private javax.swing.JTextField txtDestiny;
    private javax.swing.JTextField txtOrigin;
    // End of variables declaration//GEN-END:variables
}
