/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jm.translatorinscreen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author juan
 */
public class GoogleTranslator {
       
   
    private String apikey  = "";
    private String url = "";
    private String path = "";
    private String sourceLang = "";
    private String targetLang = "";

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }
    public GoogleTranslator() {
        this.url = "https://translation.googleapis.com";
        this.path = "/language/translate/v2";
        this.sourceLang = "en";
        this.targetLang = "es";
    }
    public GoogleTranslator(String sourceLang,String targetLang) {
        this.url = "https://translation.googleapis.com";
        this.path = "/language/translate/v2";
        this.sourceLang = sourceLang ;
        this.targetLang = targetLang;
    }
    public GoogleTranslator(String apikey) {
        this.apikey = apikey;
        this.url = "https://translation.googleapis.com";
        this.path = "/language/translate/v2";
        this.sourceLang = "en";
        this.targetLang = "es";
    }
    public GoogleTranslator(String apikey,String sourceLang,String targetLang) {
        this.apikey = apikey;
        this.url = "https://translation.googleapis.com";
        this.path = "/language/translate/v2";
        this.sourceLang = sourceLang ;
        this.targetLang = targetLang;
    }
        public String translate(String sourceText,boolean includeEnter) {
        String params="?q="+sourceText+"&source="+getSourceLang()+"&target="+getTargetLang()+"&key="+getApikey()+"&format=text";
        
        URL url;
        try {
            url = new URL(getUrl()+getPath()+params.replaceAll("\\s", "%20")); 
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            StringBuilder result = new StringBuilder ();
            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
            String line;
            while ((line = rd.readLine()) != null) {
                if(includeEnter)
                    line+="\n";
               result.append(line);
            }
            JSONObject json= new JSONObject(result.toString());
            rd.close();
            return json.getJSONObject("data").getJSONArray("translations").getJSONObject(0).get("translatedText").toString();
        } catch (MalformedURLException ex) {
            Logger.getLogger(GoogleTranslator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(GoogleTranslator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    
}
