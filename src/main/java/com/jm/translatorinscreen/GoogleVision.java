/*
 * Copyright 2017 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jm.translatorinscreen;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.json.JSONObject;

public class GoogleVision {
    private static final String fileName ="gw004.jpg";
    private File fnew;
    private BufferedWriter httpRequestBodyWriter ;
    private URL serverUrl;
    private URLConnection urlConnection ;
    private HttpURLConnection httpConnection = (HttpURLConnection)urlConnection;
    private String url = "https://vision.googleapis.com/v1/images:annotate?";
    private String apiKey;

    public GoogleVision(String apiKey) {
        httpRequestBodyWriter = null;
        serverUrl = null;
        urlConnection = null;
        this.apiKey="key="+apiKey;
    }
    
    public GoogleVision(File fnew,String apiKey, BufferedWriter httpRequestBodyWriter, URL serverUrl, URLConnection urlConnection) {
        this.fnew = fnew;
        this.httpRequestBodyWriter = httpRequestBodyWriter;
        this.serverUrl = serverUrl;
        this.urlConnection = urlConnection;
        this.apiKey = apiKey;
    }
    
    /**
     * start the connection to  https://vision.googleapis.com/v1/images:annotate? wih the apikey.
     * Use mehod POST and do a query to the API with TEXT_DETECTION
     */ 
    public boolean startConnection(){
             try {//making the URL
                 serverUrl = new URL(url + apiKey);
             } catch (MalformedURLException ex) {
                 System.out.println("the url is marlformed: "+ex);
                 return false;
             }
             try {//making a connection
                 urlConnection = serverUrl.openConnection();
             } catch (IOException ex) {
                 Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
                return false;             
             }
            this.httpConnection = (HttpURLConnection)urlConnection;
             try {//making the method of connection
                httpConnection.setRequestMethod("POST");
                httpConnection.setRequestProperty("Content-Type", "application/json");
                httpConnection.setDoOutput(true);
             } catch (ProtocolException ex) {
                 Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
                return false;
             }
             setWriter();
             return true;
    }
     /**
      * setting the format of the query for TEXT_DETECTION in a IMAGE
      */
    private void setWriter(){
        try {
            httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(httpConnection.getOutputStream()));
            httpRequestBodyWriter.write ("{\"requests\":  [{ \"features\":  [ {\"type\": \"TEXT_DETECTION\""
                    +"}], \"image\":" +" {\"content\":\""+getBytesOfImage()+"\"}}]}");
            httpRequestBodyWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * check if the response code of connection is correct (200)
     * @return 200 if is ok, else -1
     */
    private int haveStream(){
        try {
            int response = httpConnection.getResponseCode();
            System.out.println("response code" + response);
            if (httpConnection.getInputStream() == null && response != 200) {
                System.out.println("No stream");
                return -1;
            }else return response;
        } catch (IOException ex) {
            Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    /**
     * open the File that content a image .jpg
     * @return the bytes of the image, if not found return ""
     */
    private String getBytesOfImage(){
        if(!fnew.exists())return "";
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( ImageIO.read(fnew), "jpg", baos );
            return  Base64.encode(baos.toByteArray());
        } catch (IOException ex) {
            Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }
    /**
     * read the OutPutStream of the connection, get the JSON and return the description of the image.
     * @return the text founded in the image
     * @throws IOException 
     */
    public String getDescription() throws IOException{
        BufferedReader rd = null;
        StringBuilder result = new StringBuilder ();
        try {
            if(this.haveStream()<0){
                return " ";
            }
            rd = new BufferedReader(new InputStreamReader(httpConnection.getInputStream(),"UTF-8"));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            JSONObject json= new JSONObject(result.toString());
            rd.close();
            if(json.getJSONArray("responses").getJSONObject(0).has("fullTextAnnotation")){
                 return json.getJSONArray("responses").getJSONObject(0).getJSONObject("fullTextAnnotation").getString("text");
            }
            return "";
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rd.close();
            } catch (IOException ex) {
                Logger.getLogger(GoogleVision.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result.toString();
    }
    /**
     * set the image File to obtain the text of him
     * @param fileName String name of file,(can be with extention or not, admit "jpg")
     * @return true if the file exist
     */
    public boolean setImageFile(String fileName){
        if(!fileName.equals("")){//if not empty, save into a file
            String extention="jpg";
            if(!NameHaveExtention(fileName)){
                fileName+="."+extention;
            } 
            //else it will changed by the name of the file
           fnew =new File(fileName);
            return fnew.exists();
        }
        return false;
    }
    /**
     * function to recognize if the string have an extention in, like "example.jpg".
     * If it have the extention in her name, gonna be her extention in the file, else
     * the default extention is JPG
     * Seek for image formats
     * @param name String to search
     * @return  true if found an image format
     */
    public boolean NameHaveExtention(String name){
         //recognize if the name have an extention
        String[] arr_name=name.split("\\."); //use \\ to escape the dot, because the REGEX
        String[] formats = ImageIO.getReaderFormatNames();
        for (String format : formats) {
            if (format.equals(arr_name[arr_name.length-1])) {//search at the end
                return true;
            }
        }
        return false;
    }   
   
}
    